FROM python:3.9

RUN python -m pip install -U pip

COPY requirements.txt /app/requirements.txt

WORKDIR /app

RUN python -m pip install --user -r  requirements.txt

COPY ./src /app

ENTRYPOINT exec python -u billing_service.py
