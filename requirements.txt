fastapi==0.85.1
SQLAlchemy==1.4.42
asyncpg==0.26.0
pytest==7.0.1