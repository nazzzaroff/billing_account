import pytest

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from src.db_service import DBService, Account, Deposite

@pytest.fixture
def engine_path():
    return 'sqlite:///test.db'

@pytest.fixture
def engine(engine_path):
    return create_engine(engine_path)

@pytest.fixture
def session(engine):
    return sessionmaker(bind=engine)()

@pytest.fixture
def db_service(engine_path):
    return DBService(engine_path)

@pytest.fixture
def account():
    return Account(name="test1")

@pytest.fixture
def deposite(account):
    return Deposite(value=200, operation="add", account=account)
