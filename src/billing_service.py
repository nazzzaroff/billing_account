import datetime

from fastapi import Depends, FastAPI, Body, Path, Query
from fastapi.responses import JSONResponse
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import sessionmaker
from sqlalchemy import select, and_
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession

from src.db_service import Account, Base, Operation


engine = create_async_engine('postgresql+asyncpg://postgres:postgres@localhost:5432/postgres')


async def create_dbs(engine):
    async with engine.begin() as conn:
        # await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)


app = FastAPI()


async def get_db():
    async_session = sessionmaker(
        engine, class_=AsyncSession, expire_on_commit=False
    )
    async with async_session() as session:
        yield session


async def get_account_from_db(name, db: AsyncSession = Depends(get_db)):
    result = await db.execute(select(Account).filter(Account.name == name))
    return result.scalars().first()


@app.on_event("startup")
async def startup():
    await create_dbs(engine)


@app.post("/accounts")
async def create_account(data  = Body(), db: AsyncSession = Depends(get_db)):
    acc_name = data.get("name")
    acc_value = data.get("balance", 0)
    if acc_name is None:
        return JSONResponse(status_code=404, content={"message": "Имя аккаунта не задано"})
    account = Account(name=acc_name, balance=acc_value)
    db.add(account)
    await db.commit()
    await db.refresh(account)
    return account


@app.get("/accounts")
async def get_accounts(db: AsyncSession = Depends(get_db)):
    result = await db.execute(select(Account))
    return result.scalars().all()


@app.get("/accounts/{name}")
async def get_account(name, db: AsyncSession = Depends(get_db)):
    result = await get_account_from_db(name, db)
    if result is None:
        return JSONResponse(status_code=404, content={"message": "Аккаунт не существует"})
    return result


@app.post("/accounts/{name}/operation")
async def create_operation(name, data = Body(), db: AsyncSession = Depends(get_db)):
    account = await get_account_from_db(name, db)
    if account is None:
        return JSONResponse(status_code=404, content={"message": "Аккаунт не существует"})
    op_name = data.get("name")
    value = data.get("value", 0)
    if op_name is not None:
        if op_name == "Снятие":
            if value > account.balance:
                return JSONResponse(status_code=400, content={"message": "Недостаточно средств"})
            account.balance -= value
        elif op_name == "Внесение":
            account.balance += value
        else:
            return JSONResponse(status_code=400, content={"message": "Не поддерживаемый тип операции"})
    else:
        return JSONResponse(status_code=400, content={"message": "Не поддерживаемый тип операции"})
    operation = Operation(value=value, name=op_name, reason=data.get("reason", ""), 
                          account_id=account.id)
    db.add(operation)
    await db.commit()
    await db.refresh(operation)
    return operation
    

@app.get("/accounts/{name}/balance")
async def get_balance(name, db: AsyncSession = Depends(get_db)):
    account = await get_account_from_db(name, db)
    if account is None:
        return JSONResponse(status_code=404, content={"message": "Аккаунт не существует"})
    return JSONResponse(content={"balance": account.balance})


@app.get("/accounts/{name}/operations")
async def get_account_operations(
        name: str = Path(),
        start_date: datetime.datetime = Query(),
        end_date: datetime.datetime = Query(),
        db: AsyncSession = Depends(get_db)):
    account = await get_account_from_db(name, db)
    if account is None:
        return JSONResponse(status_code=404, content={"message": "Аккаунт не существует"})
    operations = await db.execute(select(Operation).filter(
        and_(
            Operation.date >= start_date, Operation.date <= end_date),
        Operation.account_id == account.id
    ))
    return operations.scalars().all()


@app.get("/operations")
async def get_operations(
        start_date: datetime.datetime = Query(),
        end_date: datetime.datetime = Query(),
        db: AsyncSession = Depends(get_db)):
    accounts = await db.execute(select(Account))
    accounts = accounts.scalars().all()

    result = list()
    for account in accounts:
        operations = await db.execute(select(Operation).filter(
            and_(
                Operation.date >= start_date, Operation.date <= end_date),
            Operation.account_id == account.id  
        ))
        operations = operations.scalars().all()
        for operation in operations:
            msg = {
                "account": account.name,
                "operation_value": operation.value
            }
            if operation.name == "Внесение":
                msg["is_deposit"] = True
            else:
                msg["reason"] = operation.reason
            result.append(msg)
    return result
